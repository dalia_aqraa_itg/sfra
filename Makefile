
include .env
export $(shell sed 's/=.*//' .env)

sign-in:
	cd build/code-build;\
	npm run client:auth;\

build-cartridge:
	rm -f ./build/code-build/output/$${CARTRIDGE_BUILD_FILE_NAME}/code/*.zip || true;\
	export BUILD_VERSION_ID=$${BUILD_VERSION_SAKS}-$${BUILD_NUMBER};\
  export ENV_TYPE=$${ENV_TYPE};\
	cd build/code-build;\
	./node_modules/.bin/grunt build --project=$${CARTRIDGE_BUILD_FILE_NAME};\

deploy-and-activate-cartridge:
	test ${SANDBOX_URL};\
	export BUILD_VERSION_ID=$${BUILD_VERSION_SAKS}-$${BUILD_NUMBER};\
	cd build/code-build;\
	npm run client:auth;\
	npx sfcc-ci code:deploy -i $${SANDBOX_URL} ./output/$${CARTRIDGE_BUILD_FILE_NAME}/code/$${BUILD_VERSION_ID}.zip;\
	npx sfcc-ci code:activate $${BUILD_VERSION_ID} -i $${SANDBOX_URL};\

delete-output-dir:
	rm -rf ./build/code-build/output/ || true;\

thebay-catalog-import:
	test ${SANDBOX_URL};\
	cd ../sfccdemodata/demo_data/hbc/ && zip -rX ../../../sfcc/build/instance-initializer/demo_data_thebay_reduced.zip demo_data_thebay_reduced;\
	cd ../../../sfcc/build/instance-initializer;\
	npm run client:auth;\
	npx sfcc-ci instance:upload demo_data_thebay_reduced.zip -i $${SANDBOX_URL};\
	npx sfcc-ci instance:import demo_data_thebay_reduced.zip -s -i $${SANDBOX_URL};\

instance-upload-and-import:
	test ${SANDBOX_URL};
	rm -f ./site_template/$${SITE_TEMPLATE_NAME}.zip || true;\
	cd site_template;\
	zip -rX $${SITE_TEMPLATE_NAME}.zip ./$${SITE_TEMPLATE_NAME};\
	cd ..;\
	cd build/code-build;\
	npm run client:auth;\
	npx sfcc-ci instance:upload ../../site_template/$${SITE_TEMPLATE_NAME}.zip -i $${SANDBOX_URL};\
	npx sfcc-ci instance:import $${SITE_TEMPLATE_NAME}.zip -i $${SANDBOX_URL} -s;\
	npx sfcc-ci job:run RebuildURLs -s -i $${SANDBOX_URL};\
	npx sfcc-ci job:run Reindex -s -i $${SANDBOX_URL};\
